newForEach = (array, callback) => {
    for(let i = 0; i < array.length; i++)
        callback(array[i], i, array)
}

newMap = (array, callback) => {
    let newArr = [];
    for(let i = 0; i < array.length; i++)
        newArr.push(callback(array[i], i, array))
        
    return newArr;
}

newSome = (array, callback) => {
    for(let i = 0; i < array.length; i++)
        if(callback(array[i], i, array)) return true;

    return false;
}

newFind = (array, callback) => {
    for(let i = 0; i < array.length; i++)
        if(callback(array[i])) return array[i] 
}

newFindIndex = (array, callback) => {
    for(let i = 0; i < array.length; i++)
        if(callback(array[i], i, array)) return i;

    return -1;
}

newEvery = (array, callback) => {
    for(let i = 0; i < array.length; i++)
        if(!callback(array[i], i, array)) return false;

    return true;
}

newFilter = (array, callback) => {
    newArr = [];
    for(let i = 0; i < array.length; i++)
        if(callback(array[i], i, array)) newArr.push(array[i]);
    return newArr;
}